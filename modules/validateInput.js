const Joi = require('joi');
const { isValidTimestamp } = require('../utilities/fieldValidation');

const schema = {
  validateGetInput: Joi.object({
    productId: Joi.string().required(),
    EventType: Joi.string(),
    EventName: Joi.string().valid('update', 'login', 'logout'),
    EventValue: Joi.object(),
    clientId: Joi.string().required(),
    timestamp: Joi.object({
      to: Joi.date().iso(),
      from: Joi.date().iso(),
    }),
    userId: Joi.string(),
  }),
  validatePostInput: Joi.object({
    productId: Joi.string().required(),
    EventType: Joi.string().required(),
    EventName: Joi.string().required().valid('update', 'login', 'logout'),
    EventValue: Joi.object().required(),
    clientId: Joi.string().required(),
    timestamp: Joi.date().iso(),
    userId: Joi.string().required(),
  }),
};

function validatePostInput(request) {
  return schema.validatePostInput.validate(request, {
    abortEarly: true,
    allowUnknown: true,
  });
  //   let postSchema = configschema.postFields;
  //   for (let index = 0; index < postSchema.length; index++) {
  //     if (postSchema[index]['required'] == 'True' && !request[postSchema[index]['propertyName']]) {
  //       return {
  //         status: 500,
  //         message: `${postSchema[index]['propertyName']} required`,
  //       };
  //     }
  //     if (request[postSchema[index]['propertyName']]) {
  //       if (
  //         postSchema[index]['type'] == 'date' &&
  //         !isValidTimestamp(request[postSchema[index]['propertyName']])
  //       ) {
  //         return {
  //           status: 500,
  //           message: `wrong format of ${postSchema[index]['propertyName']}`,
  //         };
  //       }
  //       if (
  //         postSchema[index]['type'] != 'date' &&
  //         typeof request[postSchema[index]['propertyName']] != postSchema[index]['type']
  //       ) {
  //         return {
  //           status: 500,
  //           message: `wrong format of ${postSchema[index]['propertyName']}`,
  //         };
  //       }
  //       if (
  //         postSchema[index]['values'].length > 0 &&
  //         !postSchema[index]['values'].includes(request[postSchema[index]['propertyName']])
  //       )
  //         return {
  //           status: 500,
  //           message: `wrong value of ${postSchema[index]['propertyName']}`,
  //         };
  //     }
  //   }
  //   return { status: 200, message: 'Input parameters validated succesfully' };
}

function validateGetInput(request) {
  return schema.validateGetInput.validate(request, {
    abortEarly: true,
    allowUnknown: true,
  });
  //   let getSchema = configschema.getFields;
  //   for (let index = 0; index < getSchema.length; index++) {
  //     if (getSchema[index]['required'] == 'True' && !request[getSchema[index]['propertyName']]) {
  //       return {
  //         status: 500,
  //         message: `${getSchema[index]['propertyName']} required`,
  //       };
  //     }
  //     if (request[getSchema[index]['propertyName']]) {
  //       if (typeof request[getSchema[index]['propertyName']] != getSchema[index]['type']) {
  //         return {
  //           status: 500,
  //           message: `wrong format of ${getSchema[index]['propertyName']}`,
  //         };
  //       }
  //       if (
  //         getSchema[index]['propertyName'] == 'timestamp' &&
  //         request[getSchema[index]['propertyName']]
  //       ) {
  //         if (!request['timestamp']['to'] && !request['timestamp']['from']) {
  //           return {
  //             status: 500,
  //             message: `invalid format of ${getSchema[index]['propertyName']}`,
  //           };
  //         }
  //         if (
  //           (request['timestamp']['to'] && !isValidTimestamp(request['timestamp']['to'])) ||
  //           (request['timestamp']['from'] && !isValidTimestamp(request['timestamp']['from']))
  //         ) {
  //           return {
  //             status: 500,
  //             message: `wrong format of ${getSchema[index]['propertyName']}`,
  //           };
  //         }
  //       }
  //       if (
  //         getSchema[index]['values'].length > 0 &&
  //         !getSchema[index]['values'].includes(request[getSchema[index]['propertyName']])
  //       )
  //         return {
  //           status: 500,
  //           message: `wrong value of ${getSchema[index]['propertyName']}`,
  //         };
  //     }
  //   }
  //   if (!request.clientId) return { status: 500, message: 'clientId required' };
  //   return { status: 200, message: 'Input parameters validated succesfully' };
}

module.exports = { validatePostInput, validateGetInput };
