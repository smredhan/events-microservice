const mongoose = require('mongoose');

const eventSchema = new mongoose.Schema({
  productId: String,
  EventType: String,
  EventName: String,
  EventValue: Object,
  _timestamp: { type: Date, default: new Date() },
  userId: String,
  clientId: String,
});
const eventModel = mongoose.model('eventLogs', eventSchema);

module.exports = eventModel;
