const schema = require('../config/schema.json');
const { getHandler, postHandler } = require('../handlers');

async function routes(fastify) {
  fastify.post(schema.postevent.schema.url, schema.postevent.schema, postHandler);
  fastify.post(schema.getevent.schema.url, schema.getevent.schema, getHandler);
}

module.exports = routes;
