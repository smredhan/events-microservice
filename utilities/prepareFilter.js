const getSchema = require('../config/config').getFields;

function filterPrepare(request) {
  const dpFilter = {};

  for (let index = 0; index < getSchema.length; index++) {
    let propertyName = getSchema[index]['propertyName'];
    let propertyType = getSchema[index]['type'];

    if (propertyName != 'timestamp') {
      if (request[propertyName] && typeof request[propertyName] == propertyType) {
        dpFilter[propertyName] = request[propertyName];
      }
    } else {
      if (request.timestamp) {
        const timeFilter = {};
        if (request.timestamp.from) {
          timeFilter['$gte'] = request.timestamp.from;
        } else if (request.timestamp.to) {
          timeFilter['$lte'] = request.timestamp.to;
        }
        dpFilter['_timestamp'] = timeFilter;
      }
    }
  }
  return dpFilter;
}

module.exports = filterPrepare;
