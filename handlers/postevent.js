const { validatePostInput } = require('../modules/validateInput');
const eventLog = require('../model/eventLog');

async function postHandler(req, res) {
  const validationResponse = validatePostInput(req.body);
  console.log('validationResponse');
  console.dir(validationResponse);

  if (validationResponse.error) {
    return validationResponse.error;
  }

  const body = { ...req.body };
  const dpObject = {
    productId: body.productId,
    EventType: body.EventType,
    EventName: body.EventName,
    EventValue: body.EventValue,
    userId: body.userId,
    clientId: body.clientId,
  };
  try {
    const storedEventLog = await new eventLog(dpObject).save();
    const uploadResponse = storedEventLog.toObject();
    console.log('uploadResponse');
    console.dir(uploadResponse);
    return {
      status: 200,
      result: {
        status: 'success',
        clientId: uploadResponse.clientId,
        EventValue: uploadResponse.EventValue,
      },
    };
  } catch (error) {
    console.log(error);
    return { status: 500, message: JSON.stringify(error) };
  }
}

module.exports = postHandler;
