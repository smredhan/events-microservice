const { validateGetInput } = require('../modules/validateInput');
const eventLog = require('../model/eventLog');
const prepareFilter = require('../utilities/prepareFilter');

const DEFAULT_RECORDS_PER_PAGE = require('../config/config').DefaultRecordsPerPage;
const DEFAULT_SKIP_FACTOR = require('../config/config').DefaultSkipFactor;

async function getHandler(req, res) {
  const validationResponse = validateGetInput(req.body);
  console.log(validationResponse);
  if (validationResponse.error) {
    return validationResponse.error;
  }

  /*preparing the mongoDB filter*/
  const body = { ...req.body };
  const dpFilter = prepareFilter(body);
  let page = 1;
  const fetchOptions = {
    skip: DEFAULT_SKIP_FACTOR,
    limit: DEFAULT_RECORDS_PER_PAGE,
  };
  if (body.limit && !isNaN(body.limit)) {
    fetchOptions.limit = parseInt(body.limit);
  }

  // Default page no. is 1 used in pageination
  if (body.page && !isNaN(body.page)) {
    page = parseInt(body.page);
    const pageNumber = parseInt(body.page) - 1;
    fetchOptions.skip = fetchOptions.limit * pageNumber;
  }

  console.log('dpFilter');
  console.dir(dpFilter);
  try {
    const totalCount = await eventLog.countDocuments(dpFilter);
    if (totalCount) {
      const filteredRecords = await eventLog.find(dpFilter, {_id:0, __v:0}, fetchOptions);
      let totalPages = parseInt(totalCount / fetchOptions.limit);
      if (totalCount % fetchOptions.limit) {
        totalPages++;
      }

      return {
        status: 200,
        result: {
          page,
          status: 'success',
          total_pages: totalPages,
          total_records: totalCount,
          filtered_records: filteredRecords.length,
          data: [...filteredRecords],
        },
      };
    } else {
      // This needs to be changed based on the requirement
      return {
        status: 200,
        result: {
          page,
          status: 'success',
          total_pages: 0,
          total_records: 0,
          filtered_records: 0,
          data: [],
        },
      };
    }
  } catch (error) {
    console.log(error);
    return { status: 500, message: JSON.stringify(error) };
  }
}

module.exports = getHandler;
