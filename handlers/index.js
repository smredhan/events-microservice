const postHandler = require('./postevent');
const getHandler = require('./getevent');

module.exports = {
  getHandler,
  postHandler,
};
