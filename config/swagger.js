exports.options = {
  routePrefix: '/explorer',
  exposeRoute: true,
  swagger: {
    info: {
      title: 'Events Microservice',
      description: 'This microservice handling Events trigger',
      version: '0.0.1',
    },
    host: 'api.prv:53012',
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
  },
};
