var result = {
  DefaultSkipFactor: 0,
  DefaultRecordsPerPage: 5,
  getFields: [
    {
      propertyName: 'productId',
      type: 'string',
      required: 'False',
      values: [],
    },
    {
      propertyName: 'EventType',
      type: 'string',
      required: 'False',
      values: [],
    },
    {
      propertyName: 'EventName',
      type: 'string',
      required: 'False',
      values: ['update', 'login', 'logout'],
    },
    { propertyName: 'clientId', type: 'string', required: 'True', values: [] },
    {
      propertyName: 'timestamp',
      type: 'object',
      required: 'False',
      values: [],
    },
    { propertyName: 'userId', type: 'string', required: 'False', values: [] },
    {
      propertyName: 'EventValue',
      type: 'object',
      required: 'False',
      values: [],
    },
  ],

  postFields: [
    { propertyName: 'productId', type: 'string', required: 'True', values: [] },
    { propertyName: 'EventType', type: 'string', required: 'True', values: [] },
    {
      propertyName: 'EventName',
      type: 'string',
      required: 'True',
      values: ['update', 'login', 'logout'],
    },
    {
      propertyName: 'EventValue',
      type: 'object',
      required: 'True',
      values: [],
    },
    { propertyName: 'clientId', type: 'string', required: 'True', values: [] },
    { propertyName: 'timestamp', type: 'date', required: 'False', values: [] },
    { propertyName: 'userId', type: 'string', required: 'True', values: [] },
  ],
};

module.exports = result;
