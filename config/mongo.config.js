const signzyDBHandler = require('signzy-db-handler');

const config = {
  default: {
    url: '{{dbUrl}}',
    host: '{{host}}',
    port: '{{port}}',
    database: 'events',
    user: '{{user}}',
    username: '{{username}}',
    password: '{{password}}',
    useUnifiedTopology: true,
  },
  local: {
    url: 'mongodb://mongo.prv:27017/events',
    host: 'mongo.prv',
    port: 27017,
    database: 'events',
    user: 'root',
    username: '',
    password: '',
    useNewParser: true,
  },
  mongoKey: 'HxKD8pn2JPUMPZ0fTGc598znpyoA63',
  connString: '',
};

const dbConfigHandler = signzyDBHandler.dbConfigHandler(
  config.mongoKey,
  config.default,
  config.local,
  process.env.apiblender,
);
if (dbConfigHandler.url) {
  dbConfigHandler.settings = {};
  dbConfigHandler.settings.url = dbConfigHandler.url;
}

config.connString = signzyDBHandler.createMongoConnectionString(dbConfigHandler);
module.exports = config;
