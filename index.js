const fastify = require('fastify')({
  logger: true,
});

const cors = require('cors');
const swagger = require('./config/swagger');
const dbConnector = require('./connector/db.connector.js');
const appConfig = require('./config/app.config.json');
const packageJSON = require('./package.json');
// const dbConnector = require("./connector/db.connector.js");

fastify.use(cors());

fastify.get('/info', (req, res) => {
  res.send({
    name: packageJSON.name,
    version: packageJSON.version,
    description: packageJSON.description,
  });
});

fastify.register(require('fastify-swagger'), swagger.options);

fastify.register(require('./routes/events'));

fastify.setNotFoundHandler((request, reply) => {
  reply.code(403).type('application/json').send({
    statusCode: 403,
    status: 403,
    message: 'Forbidden',
  });
});

fastify.listen(appConfig.port, appConfig.host, function (err, address) {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
  fastify.swagger();
  fastify.log.info(`Environment is:' ${process.env.NODE_ENV}`);
  fastify.log.info(`server listening on ${address}`);
  fastify.log.info(`Browse your REST API at %s%s ${address}/explorer`);
});
